---
title: 'Our Difference'
weight: 2
background: ''
button: 'About Us'
buttonLink: 'about'
---

We are "the blockchain" enthusiasts, this place is to share our experience and knowledge with anyone who feels the same. We do not like banks too much so we are always looking for a new opportunity to share how to live without them. They (the banks) are necessary (for now), but we want to be ready when the future comes tomorrow. :-)
