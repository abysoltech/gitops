---
title: 'Welcome to The Farm'
date: 2022-02-27T15:37:57+07:00
heroHeading: 'ITredneck - Welcome to Small Business "farming"'
heroSubHeading: 'ITredneck is a multipurpose resource for the digital world (e.g. IT consulting, Security for your public and private cloud, Kubernetes, GitOps, CI/CD, etc.)'
heroBackground: 'images/country-road.jpg'
---
